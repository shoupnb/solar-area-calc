from flask import render_template, Blueprint
solar_calc_blueprint = Blueprint('solar_calc',__name__)

@solar_calc_blueprint.route('/')
def index():
	return render_template("index.html")
