from flask import Flask
app = Flask(__name__,
 	static_folder = './public',
 	template_folder="./static")

from templates.solar.views import solar_calc_blueprint
# register the blueprints
app.register_blueprint(solar_calc_blueprint)
