// E = A * r * H * PR

// E = Energy (kWh)
// A = Total solar panel Area (m2)
// r = solar panel yield or efficiency(%) 
// H = Annual average solar radiation on tilted panels (shadings not included)
// PR = Performance ratio, coefficient for losses (range between 0.5 and 0.9, default value = 0.75)
//https://photovoltaic-software.com/principle-ressources/how-calculate-solar-energy-power-pv-systems

export class CalculateNominalPower{
    constructor(){
        this.Energy = 0;
        this.Area = 0; //m^2
        this.r = 15; // panel yield as % - default 
        this.H = 0; //kWh/m².anual
        this.PR = 0.75 // performance ration using default of 0.75
    };

    setSolarPanelYield(r){
        this.r = r;
    };

    setSolarPerformanceRation(PR){
        this.PR = PR;
    };

    setEnergy(Area, H){
        if(Area > 0){
            this.Area = Area, this.H = H;
            this.Energy = this.Area * this.r * this.H * this.PR;
        };
        return this.Energy;
    };

    getEnergy(){
        return this.Energy;
    };
}