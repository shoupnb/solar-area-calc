
//https://api.openweathermap.org/data/2.5/solar_radiation/forecast?lat=32.724319&lon=-114.624397&appid=3852a62ac41cbb11e7d0ca4130fdaa88

// Fields in API response

// coord Coordinates from the specified location (latitude, longitude) list radiation
// radiation.ghi Cloudy sky GHI - Global Horizontal Irradiance, W/m2
// radiation.dni Cloudy sky DNI - Direct Normal Irradiance, W/m2
// radiation.dhi Cloudy sky DHI - Diffuse Horizontal Irradiance, W/m2
// radiation.ghi_cs Clear sky GHI - Global Horizontal Irradiance, W/m2
// radiation.dni_cs Clear sky DNI - Direct Normal Irradiance, W/m2
// radiation.dhi_cs Clear sky DHI - Diffuse Horizontal Irradiance, W/m2
// dt Date and time of measurements, Unix, UTC

export class SolarRadiation {

    async getRadiationIndexFromLatLong(lat, long){
        let openWeatherUrl = `https://api.openweathermap.org/data/2.5/solar_radiation/forecast?lat=${lat}&lon=${long}&appid=3852a62ac41cbb11e7d0ca4130fdaa88`;
        const response = await fetch(openWeatherUrl);
        const solar = response.json();
        return solar;
    };
}

