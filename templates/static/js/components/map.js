import * as L from 'leaflet';
import { GeoSearchControl } from 'leaflet-geosearch';
import { OpenStreetMapProvider } from 'leaflet-geosearch';
import 'leaflet-draw';
import {CalculateArea} from './calculateArea';

export class LeafletMap {
    constructor(mapId){
        const model = this;
        model.mapId = mapId;
        model.drawnItems = L.featureGroup();
        model.map = model.createMapAndControls();
        model.calculator = new CalculateArea();
        model.area = null; //m2
        model.lat = null;
        model.long = null;
        return model;
    }

    addTileLayers(){
        const model = this;
        const tileProperties = {maxNativeZoom: 19, maxZoom: 20};
        const googleSat = L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}',tileProperties);
        const osm   = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',  tileProperties);

        L.control.layers({
            'Open Street Maps': osm.addTo(model.map),
            "Google Imagery": googleSat.addTo(model.map)}, 
            { 'Draw Layer': model.drawnItems }, 
            { position: 'topright', collapsed: true }
        ).addTo(model.map);
    };

    setCentroid(geometry){
        const model = this;
        const centroid = model.calculator.getCentroidFromArea(geometry);
        model.lat = centroid.geometry.coordinates[1];
        model.long = centroid.geometry.coordinates[0];
    }

    registerDrawEvents(drawnItems){
        const model = this;
        model.map.on('draw:created', function (event) {
            var layer = event.layer;
            drawnItems.addLayer(layer);
            model.area = model.calculator.returnAreaFromGeometry(layer.toGeoJSON().geometry); //m2
            model.setCentroid(layer.toGeoJSON().geometry);
        });

        model.map.on('draw:edited', function (e) {
            var layers = e.layers;
            layers.eachLayer(function (layer) {
                model.area = model.calculator.returnAreaFromGeometry(layer.toGeoJSON().geometry); //m2
                model.setCentroid(layer.toGeoJSON().geometry);
            });
        });
    };

    addSearchControl(){
        const model = this;
        const search = new GeoSearchControl({
            provider: new OpenStreetMapProvider(),
        });
        model.map.addControl(search);
    };

    createMapAndControls () {
            
            const model = this;    
            if(model.map !== undefined) {return model.map}; //already instantiated
            model.map = new L.Map( model.mapId,{ center: [39.73, -104.99], zoom: 10 });
            model.addTileLayers();
            model.drawnItems.addTo(model.map);

            model.map.addControl(new L.Control.Draw({
                edit: {
                featureGroup: model.drawnItems,
                    poly: {
                        allowIntersection: false
                    }
                },
                draw: {
                    polyline: false,
                    circle:false,
                    marker:false,
                    circlemarker: false,
                    polygon: {
                        allowIntersection: false,
                        showArea: true
                    }
                }
            }));

        model.registerDrawEvents(model.drawnItems);
        model.addSearchControl();
        return model.map;
    }
}
