import  area from '@turf/area';
import centroid from '@turf/centroid';

export class CalculateArea{
    returnAreaFromGeometry(geometry){
        try{
            return area(geometry);
        } 
        catch(e){
            console.exception(e);
        }
    };

    getCentroidFromArea(geometry){
        try{
            return centroid(geometry);
        } 
        catch(e){
            console.exception(e);
        }
    };
}