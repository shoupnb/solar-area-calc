import { LeafletMap } from './components/map';
import {SolarRadiation} from './components/solarRadiation';
import { CalculateNominalPower } from './components/calculateNominalPower';
const leafMap = new LeafletMap('mapid');
const solarRadiation = new SolarRadiation();
const calculatorNominalPower = new CalculateNominalPower();

Vue.component('solar-calc-form', {
    data: function () {
      return {
        map: null,
        solarData: {
            Energy : 0, //Wh/annual
            Area : 0, //m^2
            r : 15, // panel yield as %  - default value
            H : 0, //Wh/m². annual
            PR : 0.75 // performance ratio using default of 0.75
        }
      }
    },
    methods: {
        getSolarRadiation: function(){
            const vm = this;
            calculatorNominalPower.setSolarPanelYield(vm.solarData.r);
            calculatorNominalPower.setSolarPerformanceRation(vm.solarData.PR);
            if( vm.map.lat !== null && vm.map.lat !== undefined && vm.map.long !== null && vm.map.long !== undefined){
                vm.solarData.Area = vm.map.area;
                let output = solarRadiation.getRadiationIndexFromLatLong(vm.map.lat, vm.map.long);
                output.then(data => {
                    let count = 0, total = 0;
                    data.list.forEach(element => {
                        if(element.radiation.dni_cs > 0 ){
                            count++;
                            total += element.radiation.dni_cs;
                        }
                    });
                    vm.solarData.H = total/count;
                    vm.solarData.Energy = calculatorNominalPower.setEnergy(vm.map.area, vm.solarData.H)/100 //convert watts into kW;
                });
            }
        }
    },
    mounted() {
        const vm = this;
        vm.map = leafMap;
    },
    template: `
    <div class="form-inline">
    <div class="row">
            <div class="form-group" style="padding:15">
            <label for="r">Solar Panel Yield as %</label>
            <input type="number"
            id="solarPanelYield"
            class="form-control"
            v-model="solarData.r" min="0" max="100">
        </div>
        <div class="form-group" style="padding:15">
            <label for="PR">Performance Ratio</label>
            <input type="number"
            id="performanceRation"
            class="form-control"
            v-model="solarData.PR" min="0" max="1">     
        </div>
        <div class="form-group" style="padding:15">
            <label for="Area">Area m²</label>
            <input type="number"
            id="solarArea"
            class="form-control"
            v-model="solarData.Area" disabled>
        </div>  
    </div>
    <div class="row" style="padding:15">
        <button v-on:click="getSolarRadiation" class="btn btn btn-success"> Calculate Nominal Power</button>
        <div class="form-group" style="padding:15">
        <label for="Area">Energy (kW/yr)</label>
        <input disabled
        id="solarEnergy"
        class="form-control"
        v-model="solarData.Energy" disabled>

        <div class="form-group" style="padding:15">
            <label for="H">kWh/m².Annual Average &ast;</label>
            <input type="number"
            id="performanceRation"
            class="form-control"
            v-model="solarData.H" disabled>     
        </div>
        <br/>
        <span> <b> &#42; source <a  href="https://openweathermap.org/api/solar-radiation" target="_blank">Open Weather API Solar Radiation<a/> </b> </span>
    </div> 
    
    </div>
  </div>
    `
  })

  new Vue({ el: '#solar-area-calc' })