const assert = require("assert");
const internal = require("stream");
const CalculateNominalPower  = require('../js/components/calculateNominalPower');
var nominalPower = CalculateNominalPower["CalculateNominalPower"];
const nominalValue = 281250;
describe("Calculate Nominal Power Test", () => { 
    let nominal = new nominalPower();
    nominal.setEnergy(20, 1250);
    it("should calculate an appropriate nominal value of 281250 Wh/m²", () => assert.notDeepStrictEqual(nominal.Energy === nominalValue));   
    nominal.setEnergy(15, 1250);
    it("should calculate an appropriate nominal value of 281250  * 0.75 Wh/m²", () => assert.notEqual(nominal.Energy === 0.75 * nominalValue));

  });