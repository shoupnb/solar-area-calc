const assert = require("assert");
let LeafletMap  = require('../js/components/map');


addMapDiv();

describe("Test Map", () => { 
  const leaf = LeafletMap["LeafletMap"];
  const leafMap = new leaf('map');
  const map = leafMap.createMapAndControls();
  it("should not create new instance of map", () => assert(leafMap.map === map));
});

function addMapDiv () {
  // create a new div element for map
  const mapdiv = document.createElement("div");
  mapdiv.id = 'map';
  mapdiv.style.display = 'none';
  const currentDiv = document.getElementById("div1");
  document.body.insertBefore(mapdiv, currentDiv);
}