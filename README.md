# A Full Stack Web Application with Flask, NPM, Webpack for Calculating the Nominal Power Output

## Instructions
Below are the installing and running procedures
### Installing
1. Make sure you have `python`, `npm`, and `pip` installed on your machine.
For this project, I used : **npm v6.14.1**, **pip v20.0**, **python v3.7.0**
2. Enter in to the directary *area-calc-solar/templates/static/* and run the command `npm install`. This will download and install all the dependencies in *package.json*.
3. In the static directory, start the npm watcher to build the front end code with the command `npm run watch` or `npm run build`
4. Create a python virtualenv(Optional) - only necessary if not wanting to install flask globally
5. Install flask with the command `$ pip install flask`
### Running
1. Go to the root directory and start the server with `python run.py` or on deb linux `python3 run.py`
2. If all is working correctly, navigate to http://127.0.0.1:5000/ for running development application

### Unit Tests
1. Enter in to the directary *area-calc-solar/templates/static/* and run the command `npm install` if not already done in the previous section for running application. This will download and install all the dependencies in *package.json*.
2. Run `npm run-script test:mocha:watch` and go to http://localhost:8080/ for a rundown of included unit tests. Could setup `JSDOM` for running tests in a scrict node environment, but this is a nice way to a set of running tests while in active development, using `mocha`. 

### Nominal Power Output
For this designed application, all calculations used came from the following source:
https://photovoltaic-software.com/principle-ressources/how-calculate-solar-energy-power-pv-systems

From the above source calculations to derive NP are given using the following formula:

E = A * r * H * PR

E = Energy (kWh)
A = Total solar panel Area (m2)
r = solar panel yield or efficiency(%) 
H = Annual average solar radiation on tilted panels (shadings not included)
PR = Performance ratio, coefficient for losses (range between 0.5 and 0.9, default value = 0.75)

```The unit of the nominal power of the photovoltaic panel in these conditions is called "Watt-peak" (Wp or kWp=1000 Wp or MWp=1000000 Wp).```  and the following definition. 

### Third party libraries used
For the mapping portion of the application, I used [Leaflet](https://leafletjs.com/). and for spatial calculations/analysis, IE area and centroid I used [Turf](https://turfjs.org/). Both are well known opensource projects and well suited for this project. 

I started out with the `Flask` library in `python`, as I wasn't sure if I'd end up wanting to do any calculations or api calls server side, and looking back was probably a bit overkill, and could have just used webpack and vanilla js. Also used is webpack for the client side javascript build. I am using some Vue.js for a show of how MVVM could work in an application like this. I did however run into some issue with integration of vue using npm installers and the build process (IE webpack and babel) and for brevity I used the CDN, all other javascript packages can be identified in the `package.json` in *area-calc-solar/templates/static/*

